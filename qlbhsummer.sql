-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2015 at 09:55 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `qlbhsummer`
--

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

CREATE TABLE IF NOT EXISTS `binhluan` (
  `macomment` int(11) NOT NULL AUTO_INCREMENT,
  `mauser` int(11) NOT NULL,
  `mapro` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`macomment`),
  KEY `fk_mapro_ghetsanpham_sanpham` (`mapro`),
  KEY `fk_mauser_ghetsanpham_taikhoan` (`mauser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `binhluan`
--

INSERT INTO `binhluan` (`macomment`, `mauser`, `mapro`, `comment`, `time`) VALUES
(1, 2, 1, 'Xin chào', '1899-11-30 00:00:00'),
(2, 2, 1, 'Có ai ở nhà không', '1899-11-30 00:00:00'),
(3, 2, 1, 'Chiến bị ngu', '2015-12-14 11:44:31'),
(16, 1, 1, 'Hi', '2015-12-14 12:30:52'),
(17, 5, 2, 'sản phẩm quá tuyệt vời', '2015-12-15 05:16:06'),
(18, 6, 3, 'sản phẩm rất tốt cho sức khỏe....', '2015-12-15 05:30:41'),
(19, 6, 1, 'daf', '2015-12-19 10:17:30');

-- --------------------------------------------------------

--
-- Table structure for table `ghetsanpham`
--

CREATE TABLE IF NOT EXISTS `ghetsanpham` (
  `madislike` int(11) NOT NULL AUTO_INCREMENT,
  `mauser` int(11) NOT NULL,
  `mapro` int(11) NOT NULL,
  PRIMARY KEY (`madislike`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ghetsanpham`
--

INSERT INTO `ghetsanpham` (`madislike`, `mauser`, `mapro`) VALUES
(1, 1, 1),
(2, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE IF NOT EXISTS `hoadon` (
  `mabill` int(11) NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL,
  `day` datetime NOT NULL,
  `value` float NOT NULL,
  `mapro` int(11) NOT NULL,
  `mauser` int(11) NOT NULL,
  PRIMARY KEY (`mabill`),
  KEY `fk_mapro_hoadon_sanpham` (`mapro`),
  KEY `fk_mauser_hoadon_taikhoan` (`mauser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hoadon`
--

INSERT INTO `hoadon` (`mabill`, `count`, `day`, `value`, `mapro`, `mauser`) VALUES
(10, 1, '2015-12-15 05:28:33', 20, 5, 6),
(11, 1, '2015-12-16 05:57:06', 10, 2, 1),
(12, 1, '2015-12-16 05:57:33', 20, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `loai`
--

CREATE TABLE IF NOT EXISTS `loai` (
  `makind` int(11) NOT NULL AUTO_INCREMENT,
  `kind` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`makind`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `loai`
--

INSERT INTO `loai` (`makind`, `kind`) VALUES
(1, 'NƯỚC ÉP'),
(2, 'KEM'),
(3, 'CÁC SẢN PHẨM KHÁC');

-- --------------------------------------------------------

--
-- Table structure for table `loaitaikhoan`
--

CREATE TABLE IF NOT EXISTS `loaitaikhoan` (
  `maad` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`maad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `loaitaikhoan`
--

INSERT INTO `loaitaikhoan` (`maad`, `name`, `message`) VALUES
(1, 'Khách', 'Bạn có thể mua sản phẩm bất kỳ tại trang chủ'),
(2, 'Phục vụ', 'Bạn có thể vào mục thêm, xóa, sửa,thống kê'),
(3, 'Admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE IF NOT EXISTS `sanpham` (
  `mapro` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `price` float NOT NULL,
  `brands` varchar(256) CHARACTER SET utf8 NOT NULL,
  `makind` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `picture` varchar(2048) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`mapro`),
  KEY `fk_makind_sanpham_loai` (`makind`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`mapro`, `name`, `price`, `brands`, `makind`, `view`, `picture`, `description`) VALUES
(1, 'Chanh', 20000, 'FRUITI BAR', 1, 140, 'image1.jpg', 'Công dụng nước chanh:\n- Tốt cho da\n- Giải nhiệt cho cơ thể\n- Chăm sóc răng miệng\n- Chăm sóc sắc đẹp, sức khỏe'),
(2, 'Dưa hấu', 10000, 'FRUITI BAR', 1, 9, 'image2.jpeg', 'Công dụng nước ép dưa hấu:\n- Nước ép dưa hấu cung cấp lượng calo thấp\n- Hỗ trợ cho xương chắc khỏe\n- Nước giải khát mùa hè\n- Tăng cường năng lượng\n- Giúp phục hồi sự đau nhức cơ bắp và mệt mỏi'),
(3, 'Kiwi', 10000, 'FRUITI BAR', 1, 17, 'image3.jpg', 'Công dụng nước kiwi:\r\n- Giúp điều chỉnh huyết áp\r\n- Tăng cường hệ thống miễn dịch\r\n- Giúp giảm cân\r\n- Giúp loại bỏ độc tố ra khỏi cơ thể\r\n- Chống lại bệnh tim'),
(4, 'Việt quất', 20000, 'FRUITI BAR', 1, 9, 'image4.jpeg', 'Tăng cường sức đề kháng, chống oxy hóa, ngừa ung thư, tránh các bệnh nhiễm trùng đường tiết niệu, thải độc tố ra khỏi cơ thể, kháng khuẩn'),
(5, 'Nho', 20000, 'FRUITI BAR', 1, 30, 'image5.jpeg', 'Nho chứa hàm lượng cao chất xơ. Nước nho ép chứa hàng chục chất dinh dưỡng có tác dụng chống ung thư và bệnh tim. Loại trái cây này giúp tăng cường hệ thống miễn dịch bằng cách làm tăng số lượng tế bào gama và delta T trong cơ thể. Hãy thử sử dụng nước ép nho để thoát khỏi bệnh cảm lạnh.'),
(6, 'Lê', 20000, 'FRUITI BAR', 1, 20, 'image6.jpeg', 'Lê là một loại quả hết sức quen thuộc với mọi người. Bên cạnh là một loại quả ngon miệng, lê còn có rất nhiều vitamin C, flavonoid, chất xơ, khoáng chất boron mang lại những lợi ích tuyệt vời cho sức khỏe như giảm nguy cơ mắc bệnh tim, đột quỵ, tăng cường hệ miễn dịch, chống mắc bệnh tiểu đường, ung thư, hỗ trợ chị em trong quá trình giảm cân, đẹp da....'),
(7, 'Dừa', 15000, 'FRUITI BAR', 1, 4, 'image7.jpg', 'Nước dừa giúp làm đẹp da, tăng cường sức khỏe cho tim mạch, tăng cường năng lượng, giảm nguy cơ mất nước.....'),
(8, 'Khoai môn', 15000, 'FRUITI BAR', 1, 18, 'image8.jpg', 'Khoai môn cung cấy đầy đủ các chất đạm, tinh bột, các loại vitamin A, C, B… giúp cơ thể con người chống lại các chất gây lão hóa da, gia tăng thị lực, tăng cường sức đề kháng, nhuận tràng…'),
(9, 'Ốc quế', 15000, 'frutina fruit rush', 2, 19, 'image9.jpg', 'Kem ốc quế với nhiều mùi, nhiều vị, giúp giải nhiệt mùa hè....'),
(10, 'Crepe', 25000, 'frutina fruit rush', 2, 12, 'image10.jpg', 'Vỏ bánh dai dai, kem tươi ngậy, hoa quả tươi, sốt caramel, mứt dâu, sữa đặc... tất cả tạo nên một chiếc crepe hoàn hảo cả về hình thức lẫn hương vị. Crepe ốc quế cũng rất tiện để takeaway...'),
(11, 'Bánh quế', 20000, 'frutina fruit rush', 2, 15, 'image11.jpeg', 'Kết hợp giữa kem với bánh quế, tạo vị lạ miệng cho bạn...'),
(12, 'Bánh mì', 20000, 'frutina fruit rush', 2, 11, 'image12.jpg', 'Bánh mì kẹp kem mát lạnh,có nhiều vị, giải nhiệt trong mùa hè nóng bức.'),
(13, 'Ly', 25000, 'frutina fruit rush', 2, 19, 'image13.jpg', 'Kem ly nhiều tầng, với nhiều hương vị khác nhau...'),
(14, 'Tô', 25000, 'frutina fruit rush', 2, 22, 'image14.jpg', 'Kem tô lạ mắt, bạn vó thể chọn nhiều loại mùi khác nhau.'),
(15, 'Trái cây', 25000, 'frutina fruit rush', 2, 20, 'image15.jpg', 'Kết hợp giữa kem và trái cây, vừa mát lạnh. vừa tốt cho sức khỏe của bạn.'),
(16, 'Sô-cô-la', 20000, 'frutina fruit rush', 2, 24, 'image16.jpg', 'Kem sô cô la nhiều tầng, thơm, béo, không chứa nhiều đường.'),
(17, 'Kem chống nắng\r\n', 55000, 'SEA SHOP', 3, 19, 'image17.jpg', 'Kem chống nắng là một vật dụng không thể thiếu cho ngày nắng gắt, giúp bảo vệ làn da của bạn khỏi tác hại của tia cực tím..'),
(18, 'Chân vịt', 70000, 'SEA SHOP', 3, 18, 'image18.jpeg', 'Chân vịt giúp bạn bơi dễ dàng như con vịt.'),
(19, 'Phao kẹp', 40000, 'SEA SHOP', 3, 12, 'image19.jpg', 'Kẹp vào 2 cánh gà giúp nổi trên mặt nước. Thích hợp với những người mới tập bơi'),
(20, 'Ván bơi', 60000, 'SEA SHOP', 3, 8, 'image20.jpeg', 'Ván tập bơi, dành cho người mới biết bơi.'),
(21, 'Ván lướt sóng', 80000, 'SEA SHOP', 3, 13, 'image21.jpeg', 'Ván lướt sóng, trải nghiệm cảm giác mạnh, lướt giữa những ngọn sóng.Bạn có muốn thử chứ !!'),
(22, 'Kính bơi', 50000, 'SEA SHOP', 3, 10, 'image22.jpg', 'Kính bơi,vừa làm đẹp, vừa nhìn rõ dưới nước, vừa bảo vệ mắt của bạn, tránh nước hay các vật lạ dưới nước va phải.'),
(23, 'Phao hình thú', 60000, 'SEA SHOP', 3, 12, 'image23.jpg', 'Phao hình thú cho bé, giúp bé nhà bạn hứng thú với việc bơi lội dưới nước...'),
(24, 'Áo phao', 50000, 'SEA SHOP', 3, 15, 'image24.jpg', 'Áo phao giúp bạn bơi thoải mái mà không sợ xảy ra trường hợp nguy hiểm như chuột rút hay đuối hơi, đuối nước.'),
(25, 'Quần bơi', 80000, 'SEA SHOP', 3, 14, 'image25.JPG', 'Quần bơi chỉ các bạn nam mặc khi đi bơi'),
(26, 'Kính râm', 50000, 'SEA SHOP', 3, 11, 'image26.jpg', 'Kính râm, giúp mắt bạn đỡ chói hơn khi ra ngoài trời nắng gắt.'),
(27, 'Phao bơi bánh xe', 80000, 'SEA SHOP', 3, 8, 'image27.jpeg', 'Phao bơi hình bánh xe rất thích hợp cho người lớn.'),
(28, 'Áo bơi', 60000, 'SEA SHOP', 3, 13, 'image28.jpeg', 'Áo bơi dành cho các bạn nữ với nhiều màu sắc'),
(29, 'Nón rộng vành', 60000, 'SEA SHOP', 3, 20, 'image29.jpg', 'Nón rộng vành, vật dụng cần thiết khi ra biển.'),
(30, 'Dép kẹp', 40000, 'SEA SHOP', 3, 14, 'image30.jpg', 'Dép kẹp, giúp bạn di chuyển dễ dàng khi đi dạo biển.'),
(31, 'Cần câu cá', 100000, 'SEA SHOP', 3, 12, 'image31.jpeg', 'Cần câu cá , hãy cố gắng câu đươc san hô nhé, biển này nhiều san hô lắm'),
(32, 'Dụng cụ lặn', 1000000, 'SEA SHOP', 3, 22, 'image32.jpg', 'Dụng cụ lặn : Bộ quần áo liền thân , Mũ lặn ,   Găng tay lặn , Áo phao cân bằng khí BCD , Áo phao cân bằng khí BCD ,Hệ thống điều chỉnh áp suất và mồm thở chính , Mồm thở phụ ,  La bàn đeo tay ,   Kính lặn ,  Ống thở lặn,  Chân nhái ,   Dao lặn chuyên dùng  , Bình thở  ,  Đèn pin lặn .');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE IF NOT EXISTS `taikhoan` (
  `mauser` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `phone` char(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `picture` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` datetime NOT NULL,
  `maad` int(11) NOT NULL,
  PRIMARY KEY (`mauser`),
  KEY `fk_maad_taikhoan_loaitaikhoan` (`maad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`mauser`, `user`, `pass`, `phone`, `email`, `sex`, `picture`, `birthday`, `maad`) VALUES
(1, 'jerry', '123456789', '01653818659', 'doduongphuonguyen@gmail.com', 1, 'avatar.png', '2008-07-19 12:00:00', 1),
(2, 'Bảo', '2233445566', '901451896', 'duongvuongngocnga@gmail.com', 0, 'chanh-trung-quoc.jpg', '1995-08-27 12:00:00', 3),
(3, 'Duy', 'dragonflame', '090167833', 'dragonflame@gmail.com', 0, 'avatar.png', '2015-12-01 00:00:00', 1),
(5, 'Bloom', 'believixWinx', '0989809878', 'Roxy@gmail.com', 1, 'avatar.png', '0000-00-00 00:00:00', 2),
(6, 'chien', '123', '980873745', 'chien@gmail.com', 0, 'Desert.jpg', '2015-04-15 05:03:00', 1),
(7, 'Roxy', '444333444', '333888333', 'Roxy@gmail.com', 1, 'Roxy.png', '1994-10-01 12:26:41', 2),
(8, 'Klaus', '10101010', '555777666', 'Klaus@gmail.com', 0, 'Klaus.jpg', '1984-10-10 12:26:41', 2),
(9, 'bupbe', '112223', '34566', 'bupbe@gmail.com', 0, 'avatar.png', '2015-12-02 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `thichsanpham`
--

CREATE TABLE IF NOT EXISTS `thichsanpham` (
  `malike` int(11) NOT NULL AUTO_INCREMENT,
  `mauser` int(11) NOT NULL,
  `mapro` int(11) NOT NULL,
  PRIMARY KEY (`malike`),
  KEY `fk_mauser_thichsanpham_taikhoan` (`mauser`),
  KEY `fk_mapro_thichsanpham_sanpham` (`mapro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `thichsanpham`
--

INSERT INTO `thichsanpham` (`malike`, `mauser`, `mapro`) VALUES
(1, 2, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `binhluan`
--
ALTER TABLE `binhluan`
  ADD CONSTRAINT `fk_mapro_ghetsanpham_sanpham` FOREIGN KEY (`mapro`) REFERENCES `sanpham` (`mapro`),
  ADD CONSTRAINT `fk_mauser_ghetsanpham_taikhoan` FOREIGN KEY (`mauser`) REFERENCES `taikhoan` (`mauser`);

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD CONSTRAINT `fk_mapro_hoadon_sanpham` FOREIGN KEY (`mapro`) REFERENCES `sanpham` (`mapro`),
  ADD CONSTRAINT `fk_mauser_hoadon_taikhoan` FOREIGN KEY (`mauser`) REFERENCES `taikhoan` (`mauser`);

--
-- Constraints for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `fk_makind_sanpham_loai` FOREIGN KEY (`makind`) REFERENCES `loai` (`makind`);

--
-- Constraints for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD CONSTRAINT `fk_maad_taikhoan_loaitaikhoan` FOREIGN KEY (`maad`) REFERENCES `loaitaikhoan` (`maad`);

--
-- Constraints for table `thichsanpham`
--
ALTER TABLE `thichsanpham`
  ADD CONSTRAINT `fk_mapro_thichsanpham_sanpham` FOREIGN KEY (`mapro`) REFERENCES `sanpham` (`mapro`),
  ADD CONSTRAINT `fk_mauser_thichsanpham_taikhoan` FOREIGN KEY (`mauser`) REFERENCES `taikhoan` (`mauser`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
