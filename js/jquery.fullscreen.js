//fullscreen
function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.getElementById("gamesfullscreen").requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.getElementById("gamesfullscreen").mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.getElementById("gamesfullscreen").webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else if ((document.fullScreenElement && document.fullScreenElement == null) ||    
   (document.mozFullScreen && document.webkitIsFullScreen)) {  
    if (document.cancelFullScreen) {  
      document.getElementById("gamesfullscreen").cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.getElementById("gamesfullscreen").mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.getElementById("gamesfullscreen").webkitCancelFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } 
}

function toggleFullScreen2() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.getElementById("gamesfullscreen2").requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.getElementById("gamesfullscreen2").mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.getElementById("gamesfullscreen2").webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else if ((document.fullScreenElement && document.fullScreenElement == null) ||    
   (document.mozFullScreen && document.webkitIsFullScreen)) {  
    if (document.cancelFullScreen) {  
      document.getElementById("gamesfullscreen2").cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.getElementById("gamesfullscreen2").mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.getElementById("gamesfullscreen2").webkitCancelFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } 
}

function toggleFullScreen3() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.getElementById("gamesfullscreen3").requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.getElementById("gamesfullscreen3").mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.getElementById("gamesfullscreen3").webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else if ((document.fullScreenElement && document.fullScreenElement == null) ||    
   (document.mozFullScreen && document.webkitIsFullScreen)) {  
    if (document.cancelFullScreen) {  
      document.getElementById("gamesfullscreen3").cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.getElementById("gamesfullscreen3").mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.getElementById("gamesfullscreen3").webkitCancelFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } 
}

function toggleFullScreen4() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.getElementById("gamesfullscreen4").requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.getElementById("gamesfullscreen4").mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.getElementById("gamesfullscreen4").webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else if ((document.fullScreenElement && document.fullScreenElement == null) ||    
   (document.mozFullScreen && document.webkitIsFullScreen)) {  
    if (document.cancelFullScreen) {  
      document.getElementById("gamesfullscreen4").cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.getElementById("gamesfullscreen4").mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.getElementById("gamesfullscreen4").webkitCancelFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } 
}

function toggleFullScreen5() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.getElementById("gamesfullscreen5").requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.getElementById("gamesfullscreen5").mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.getElementById("gamesfullscreen5").webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else if ((document.fullScreenElement && document.fullScreenElement == null) ||    
   (document.mozFullScreen && document.webkitIsFullScreen)) {  
    if (document.cancelFullScreen) {  
      document.getElementById("gamesfullscreen5").cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.getElementById("gamesfullscreen5").mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.getElementById("gamesfullscreen5").webkitCancelFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } 
}