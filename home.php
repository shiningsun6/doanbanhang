<?php
include("header.php");
?>
<!-- Main -->
<audio loop="loop"><source src="css/music/all.mp3"/></audio>
<div id="main">
	<div class="shell">
		
		<!-- Search, etc -->
		<div class="options">
			<div class="search">
				<form action="sanpham/timkiem.php" method="get">
					<span class="field"><input type="text" class="blink" value="tìm kiếm" title="tìm kiếm" name="tensp" /></span>
					<input type="submit" class="search-submit" value="tìm kiếm" />
				</form>
			</div>
			
			<div class="right">
            <span class="cart"><?php 
					if(isset($_SESSION["TenDangNhap"])) {
						echo "<a href='account.php?mauser=".$_SESSION["MaUser"]."' class='tdn'>".$_SESSION["TenDangNhap"]."</a>";	
						echo "<a href='#' class='cart-ico'>&nbsp;</a>";
						if (isset($_SESSION["tongtiengiohang"]))
						{
							echo "<strong>$".$_SESSION["tongtiengiohang"]."K</strong>";
						}
						else { echo "<strong>$0.00</strong>";}
					}
					?>
                    </span>
                    
				<span class="left more-links">
				<?php 
				 if(isset($_SESSION["DaDangNhap"])) { 
					echo '<a href="dangxuat.php">Đăng Xuất</a>';
					
				 } else {
					echo '<a href="dangnhap.php">Đăng Nhập</a>';
					echo '<a href="dangky.php">Đăng Ký</a>';
				 } 
				 ?>
				</span>
			</div>
		</div>
		<!-- End Search, etc -->
        		
		<!-- Content -->
		<div id="content">
			
			<!-- Tabs -->
			<div class="tabs">
				<ul>
				    <li><a href="#" class="active"><span>Nước ép</span></a></li>
				    <li><a href="#"><span>Kem</span></a></li>
				    <li><a href="#" class="red"><span>Các sản phẩm khác</span></a></li>
				</ul>
			</div>
			<!-- Tabs -->
			
			<!-- Container -->
			<div id="container">
				
				<div class="tabbed">
					
					<!-- First Tab Content -->
					<div class="tab-content" style="display:block;">
						<div class="items">
							<div class="cl">&nbsp;</div>
							<ul>
                            <?php
							
							$rsnuocep = laysanphamnuocep(); 
							 while(true)
							{
								$rownuocep=mysqli_fetch_array($rsnuocep);
								if($rownuocep==null)
								{
									break;
								}
							echo  "<li>";
							echo   "<div class='image'>";
							echo    "<a href='sanpham/sp1.php?mapro=".$rownuocep['mapro']."'><img src='css/images/".$rownuocep['picture']."' width='136' height='83' alt='' /></a>";
							echo    "</div>";
							echo    "<p>";
							echo    "Mã số sản phẩm: <span>".$rownuocep['mapro']."</span><br />";
							echo    "Tên: <a href=''>".$rownuocep['name']."</a><br/>";
							echo 	"<span style='color:grey;'>Lượt xem: ".$rownuocep['view']."</span>";
							echo    "</p>";
							echo    "<p class='price'>Đơn giá: <strong>".$rownuocep['price']."</strong></p>";
							echo    "</li>";
								
							}
							?>  
							</ul>
							<div class="cl">&nbsp;</div>
						</div>
					</div>
					<!-- End First Tab Content -->
					
					<!-- Second Tab Content -->
					<div class="tab-content">
						<div class="items">
							<div class="cl">&nbsp;</div>
							<ul>
							    <?php 
								$rskem = laysanphamkem();
								while(true)
								{
									$rowkem = mysqli_fetch_array($rskem);
									if($rowkem==null)
									{ break;}
							echo  "<li>";
							echo   "<div class='image'>";
							if($rowkem['mapro']==13||$rowkem['mapro']==15||$rowkem['mapro']==16)
							{
							echo    "<a href='sanpham/sp1.php?mapro=".$rowkem['mapro']."'><img src='css/images/".$rowkem['picture']."' width='80' height='83' alt='' /></a>";}
							else{
							echo    "<a href='sanpham/sp1.php?mapro=".$rowkem['mapro']."'><img src='css/images/".$rowkem['picture']."' width='136' height='83' alt='' /></a>";}
							
							echo    "</div>";
							echo    "<p>";
							echo    "Mã số sản phẩm: <span>".$rowkem['mapro']."</span><br />";
							echo    "Tên: <a href=''>".$rowkem['name']."</a><br/>";
							echo 	"<span style='color:grey;'>Lượt xem: ".$rowkem['view']."</span>";
							echo    "</p>";
							echo    "<p class='price'>Đơn giá: <strong>".$rowkem['price']."</strong></p>";
							echo    "</li>";
								
							}?>
							</ul>
							<div class="cl">&nbsp;</div>
						</div>
					</div>
					<!-- End Second Tab Content -->
					
					<!-- Third Tab Content -->
					<div class="tab-content">
						<div class="items">
							<div class="cl">&nbsp;</div>
							<ul>
							     <?php 
								$rscspk = laysanphamkhac();
								while(true)
								{
									$rowcspk = mysqli_fetch_array($rscspk);
									if($rowcspk==null)
									{ break;}
							echo  "<li>";
							echo   "<div class='image'>";
							echo    "<a href='sanpham/sp1.php?mapro=".$rowcspk['mapro']."'><img src='css/images/".$rowcspk['picture']."' width='136' height='83' alt='' /></a>";
							echo    "</div>";
							echo    "<p>";
							echo    "Mã số sản phẩm: <span>".$rowcspk['mapro']."</span><br />";
							echo    "Tên: <a href=''>".$rowcspk['name']."</a><br/>";
							echo 	"<span style='color:grey;'>Lượt xem: ".$rowcspk['view']."</span>";
							echo    "</p>";
							echo    "<p class='price'>Đơn giá: <strong>".$rowcspk['price']."</strong></p>";
							echo    "</li>";
								
							}?>
						    </ul>
							<div class="cl">&nbsp;</div>

                           
						</div>
					</div>
					<!-- End Third Tab Content -->
					
				</div>
				
				<!-- Brands -->
				<div class="brands">
					<h3>Nhãn hiệu</h3>
					<div class="logos">
						<a href="#"><img src="css/images/logo1.jpg" width="96" height="59" alt="" /></a>
						<a href="#"><img src="css/images/logo2.jpg" width="96" height="59" alt="" /></a>
						<a href="#"><img src="css/images/logo3.jpg" width="96" height="59" alt="" /></a>
						<a href="#"><img src="css/images/logo4.jpg" width="96" height="59" alt="" /></a>
						<a href="#"><img src="css/images/logo5.jpg" width="96" height="59" alt="" /></a>
                      
					</div>
				</div>
				<!-- End Brands -->
                
<?php include("footer.php");?>