<?php
include("header.php");
?>

<!-- Main -->
<audio loop="loop"><source src="css/music/all.mp3"/></audio>
<div id="main">
	<div class="shell">
		
		<!-- Search, etc -->
		<div class="options">
			<div class="search">
				<form action="sanpham/timkiem.php" method="get">
					<span class="field"><input type="text" class="blink" value="tìm kiếm" title="tìm kiếm" name="tensp" /></span>
					<input type="submit" class="search-submit" value="tìm kiếm"/>
				</form>
			</div>
			
			<div class="right">
			<span class="cart">
			<?php 
					if(isset($_SESSION["TenDangNhap"])) {
						echo "<a href='account.php?mauser=".$_SESSION["MaUser"]."' class='tdn'>".$_SESSION["TenDangNhap"]."</a>";	
						echo "<a href='#' class='cart-ico'>&nbsp;</a>";
						if (isset($_SESSION["tongtiengiohang"]))
						{
							echo "<strong>$".$_SESSION["tongtiengiohang"]."K</strong>";
						}
						else { echo "<strong>$0.00</strong>";}
					}
			?></span>
                    
				<span class="left more-links">
					<?php 
				 if(isset($_SESSION["DaDangNhap"])) { 
					echo '<a href="dangxuat.php">Đăng Xuất</a>';
					
				 } else {
					echo '<a href="dangnhap.php">Đăng Nhập</a>';
					echo '<a href="dangky.php">Đăng Ký</a>';
				 } 
				 ?>
				</span>
			</div>
		</div>
		<!-- End Search, etc -->
        		
		<!-- Content -->
		<div id="content">
			
			<!-- Tabs -->
			<div class="tabs">
				<ul>
				     <li><a href="#" class="active"><span>Câu hỏi thường gặp</span></a></li>
				    <li><a href="#"><span>Hướng dẫn sử dụng</span></a></li>
				</ul>
			</div>
			<!-- Tabs -->
			
			<!-- Container -->
			<div id="container">
				<div style="height:500px;">
				<div class="tabbed">
				
					<!-- First Tab Content -->
					<div class="tab-content" style="display:block;">
                    <div class="mainbanner">
					  <ul class="banner">Làm thế nào để mình mua sản phẩm?
                       	<li  class="liforbanner" >Bạn cần đăng nhập để có thể thực hiện chức năng này. Thân!</li>
                      </ul>
                        <ul class="banner1" >Mình có thể xóa tài khoản khi không còn sử dụng?
                       	  <li class = "liforbanner2" >Bạn liên hệ với admin cửa hàng để xóa tài khoản.Thân!</li>
                      </ul>
                       <ul class="banner2">Mình liên hệ với cửa hàng như thế nào?
                   		 <li class = "liforbanner3">Trên cùng góc phải có link liên hệ <img src="css/images/hd1.jpg" width="200"/>, bên dưới <img src="css/images/hd2.jpg" width="200"/>bạn bấm vào mục liên hệ và liên lạc với cửa hàng.Thân!</li>
                      </ul>
                       <ul class="banner3">Ngoài những sản phẩm có trong cửa hàng, có những sản phẩm khác không?
                      	 <li class ="liforbanner4">Cửa hàng của mình chỉ bán những sản phẩm này.Thân!</li>
                      </ul>
                       <ul class="banner4">Trong trang web có giờ không bạn?
                      	 <li class ="liforbanner5">Giờ ở cuối góc phải trang đó.Thân!</li>
                      </ul>
                      </div>
                       </div>
					<!-- End First Tab Content -->
					
				  <!-- Second Tab Content -->
					<div class="tab-content">
                    	<div class="phim">
							<iframe width="750" height="315" src="https://www.youtube.com/embed/evpl6e-Jvh0" frameborder="0" allowfullscreen></iframe>		
                        <div class="menu">
                        </div>
					</div>
				  <!-- End Second Tab Content -->
					
				
					
				</div>
<?php include("footer.php");?>